as yet unclassified
pollInstances
	"Check for completed tasks and handle completion/error states."
	| newInstances |
	newInstances := runningInstances
				select: [:instance | instance isRunning].
	runningInstances
		do: [:instance | instance isRunning
				ifFalse: [self
						tellUser: ('Lisp process {1} has completed.' format: {instance pid}).
					instance errorFileExists
						ifTrue: [self handleError: instance receiveLispError].
					instance stopLisp]].
	runningInstances := newInstances