as yet unclassified
processAll
	| stream string |
	startTime := Time now.
	self hasFailed: false.
	isCompleted := false.
	self createWorkerProcess.
	self tellUser: 'Processing...'.
	string := String new.
	stream := WriteStream on: string.
	stream nextPutAll: 'Starting at '.
	startTime printOn: stream.
	self tellUser: stream contents.