as yet unclassified
createWorkerProcess
	"Start a Squeak process to manage execution of the task list."
	^ workerProcess := self workerBlockClosure forkNamed: (self class name).