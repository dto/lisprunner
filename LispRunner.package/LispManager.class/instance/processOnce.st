as yet unclassified
processOnce
	"This is the main loop: check all processes, handle errors and events, 
spawn the next Lisp process if any, and then notify the user of completion."
	self pollInstances.
	runningInstances
		do: [:instance | 
			| output |
			output := instance tick.
			instance handleEvents.
			instance errorFileExists
				ifTrue: [self handleError: instance receiveLispError].
			output
				ifNil: [ self showProgress ]
				ifNotNil: [ self showOutput: output. 
					self showProgress ]].
	runningInstances
		ifEmpty: [lispQueue
				ifEmpty: []
				ifNotEmpty: [self spawnNext]].
	(self isComplete and: hasFailed not)
		ifTrue: [isCompleted
				ifFalse: [self reportCompleted.
					isCompleted := true]]