as yet unclassified
isComplete
	"Return true if all tasks have completed without errors."
	^ (runningInstances isEmpty and: lispQueue isEmpty)
		and: hasFailed not