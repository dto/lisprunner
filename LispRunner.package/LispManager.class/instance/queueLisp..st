as yet unclassified
queueLisp: expression 
	"Add a Lisp expression to the queue."
	expression size > 0
		ifTrue: [lispQueue add: expression]