as yet unclassified
workerBlockClosure
	"This is the body of the Lisp process watcher."
	^ [[self isComplete not]
		whileTrue: [self tick.
			(Delay forMilliseconds: 500) wait.
			Processor yield]]