as yet unclassified
cancelAllJobs 
	"Stop all processing and clear the task list."
	hasFailed := true.
	isCompleted := false.
	lispQueue := OrderedCollection new.
	runningInstances
		do: [:instance | instance stopLisp].
	runningInstances := OrderedCollection new.
	self tellUser: 'All tasks canceled.'