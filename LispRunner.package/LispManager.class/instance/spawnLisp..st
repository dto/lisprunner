as yet unclassified
spawnLisp: lisp
	"Create a new Lisp instance and execute the argument code."
	| runner |
	runner := self createRunner.
	runner writeLisp: (runner prepareLisp: lisp).
	runner startLisp.
	self tellUser: 'Started Lisp process with ID ' , runner lispProcess processProxy pid, '.'.
	self runningInstances addFirst: runner.
