as yet unclassified
spawnNext
	"Start the next Lisp job in the queue. If a job is itself an
	OrderedCollection of Strings (instead of a single String) then the multiple
	jobs are started in parallel."
	| lisp |
	lispQueue
		ifEmpty: [^ nil].
	lisp := lispQueue at: 1.
	lisp isString
		ifTrue: [lisp size > 0
				ifTrue: [self spawnLisp: lisp]]
		ifFalse: [lisp
				do: [:s | self spawnLisp: s]].
	lispQueue removeAt: 1