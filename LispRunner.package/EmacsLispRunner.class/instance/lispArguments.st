as yet unclassified
lispArguments
	| args |
	args := {'-Q' . '--no-desktop' . '--batch' . '--eval' } asOrderedCollection.
	args addLast: '(load "', self commandFile asString, '")'.
 	^ args
