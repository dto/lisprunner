as yet unclassified
lispPrelude
	^ '(defvar *lisprunner-project-directory* "")

(defun lisprunner-project-file (file)
  (expand-file-name (file-name-as-directory
                     *lisprunner-project-directory*)
                    file))

(defvar *lisprunner-return-file* "")

(defvar *lisprunner-error-file* "")

(defun sexp->file (sexp file)
  (with-temp-buffer
    (insert (format "%S" sexp))
    (write-file file)))

(defun lisprunner-write-return-file (data)
  (sexp->file data *lisprunner-return-file*))

(defun lisprunner-write-error-file (data)
  (sexp->file data *lisprunner-error-file*))
  
(defmacro lisprunner-catch-errors (form)
  `(lisprunner-write-return-file
    (condition-case -_-_- (progn (funcall ,form))
      (error (lisprunner-write-error-file (prin1-to-string -_-_-))))))
'	