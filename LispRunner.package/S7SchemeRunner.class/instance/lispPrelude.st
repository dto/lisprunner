as yet unclassified
lispPrelude
	^ '(define *lisprunner-project-directory* "")

(define (lisprunner-project-file file)
  (string-append *lisprunner-project-directory* "/" file))

(define *lisprunner-return-file* "")

(define *lisprunner-error-file* "")

(define (sexp->file sexp file)
  (let ((p (open-output-file file)))
    (format p "~W" sexp)
    (close-output-port p)))

(define (lisprunner-write-return-file data)
  (sexp->file data *lisprunner-return-file*))

(define (lisprunner-write-error-file data)
  (sexp->file data *lisprunner-error-file*))
  
(define (lisprunner-catch-errors thunk)
  (catch #t (lambda ()
              (lisprunner-write-return-file (thunk)))
    (lambda args
      (let ((text (format #f "~A: ~A~%~A[~A]:~%~A~%" 
                          (car args)                        
                          (apply format #f (cadr args))     
                          (port-filename) (port-line-number)
                          (stacktrace))))                   
        (display text (current-error-port))
        (lisprunner-write-error-file text)))))
'