as yet unclassified
tick
	| output lines |
	lispOutput ifNotNil: [
		lispOutput reader flush.
		lispProcess pipeFromError reader flush.
		lispProcess pipeFromError writer flush.
		lispProcess pipeFromError upToEnd.
		output := ''.
		[lispOutput atEnd] whileFalse: [output := output, lispOutput upToEnd]]. 
	output size > 0 ifTrue: 
		[ lines := output splitBy: (String with: Character cr). 
			^ output].
	^ nil