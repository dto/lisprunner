as yet unclassified
isRunning
	lispProcess ifNotNil: [^ lispProcess isRunning]
					ifNil: [^ false]