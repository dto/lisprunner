as yet unclassified
writeLisp: lisp
	| stream |
	FileDirectory default deleteFileNamed: self commandFile.
	self assert: (FileDirectory default fileExists: self commandFile) not. 
	stream := FileDirectory default fileNamed: self commandFile.
	stream nextPutAll: lisp; close.
	^ stream fullName.
