as yet unclassified
loadSourceForms
	| files forms | 
	files := self loadSourceFiles.
	forms := ''.
	files do: [ :file | forms := forms , ' (load "', file, '") ' ].
	^ forms