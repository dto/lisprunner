as yet unclassified
receiveLispError
	| stream output |
	self errorFileExists
		ifTrue: [
			stream := FileDirectory default readOnlyFileNamed: self errorFile.
			output := stream contents asString.
			stream close.
			self deleteErrorFile.
			^ output]
		ifFalse: [^ nil]