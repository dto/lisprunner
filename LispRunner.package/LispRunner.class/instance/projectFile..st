as yet unclassified
projectFile: filenameWithoutDirectory 
	^ (FileDirectory default directoryNamed: projectPath) fullNameFor: filenameWithoutDirectory.