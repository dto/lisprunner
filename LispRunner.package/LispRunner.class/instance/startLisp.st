as yet unclassified
startLisp
	| args |
	args := self lispArguments.
	lispProcess ifNil: [
		lispProcess := PipeableOSProcess
			new: lispExecutable 
			arguments: args
			environment: (self lispEnvironment)
			descriptors: nil
			workingDir: (LispRunner temporaryPath fullName)
			errorPipelineStream: nil].
	"actually start the process now"
	lispProcess
		prepareOutputForInternalReader;
		value.
	lispOutput := lispProcess pipeFromOutput.
	lispOutput reader ascii.
	pid := lispProcess processProxy pid.
