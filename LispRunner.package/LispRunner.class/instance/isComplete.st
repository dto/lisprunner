as yet unclassified
isComplete
	lispProcess
		ifNil: [^ false]
		ifNotNil: [^ lispProcess isComplete]