as yet unclassified
sourceFile: filenameWithoutDirectory 
	^ (FileDirectory default directoryNamed: sourcePath) fullNameFor: filenameWithoutDirectory.