as yet unclassified
prepareLisp: commandString
	"Collect and emit the lispSpool forms and the commandString, wrapped in source-loading and error handling forms.
	Answers the resulting complete Lisp program as a String.
	The 'prelude' contains Lisp definitions for embedded lisprunner functions. It is defined by each Lisp dialect's subclass.
	See also EmacsLispRunner>>#lispPrelude and S7SchemeRunner>>#lispPrelude.
	The 'interlude' contains extra Lisp definitions that must be run before the lispSpool forms. 
	See also EmacsLispRunner>>#lispInterlude.
	"
	| command |
	self tellLispPreamble.
	self tellLispBody.
	command := '(' , self lispBegin , ' ', self lispPrelude ,  self loadSourceForms, ')' , (self lispInterlude), '(lisprunner-catch-errors (lambda () (' , self lispBegin, ' '.
	self lispSpool reversed do: [:each | command := command, each, ' '].
	self lispSpool: OrderedCollection new.
	 ^ command, commandString, ')))', self lispPostamble.
	

	
