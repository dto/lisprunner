as yet unclassified
receiveLisp
	| stream output |
	self returnFileExists
		ifTrue: [
			stream := FileDirectory default readOnlyFileNamed: self returnFile.
			output := stream contents asString.
			stream close.
			self deleteReturnFile.
			^ output]
		ifFalse: [^ nil]