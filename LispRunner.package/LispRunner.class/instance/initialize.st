initialization
initialize
	super initialize.
	self initializeDefaults.
	lispSpool := OrderedCollection new.
	IdSequenceNumber ifNil: [IdSequenceNumber := 1 ]
						ifNotNil: [IdSequenceNumber := IdSequenceNumber + 1 ].
	self id: (IdSequenceNumber asString, '-' , 65536 random asString).
	