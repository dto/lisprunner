as yet unclassified
temporaryPath
^ OSProcess isUnix ifTrue: [FileDirectory default directoryNamed: '/tmp']
						ifFalse: [FileDirectory default directoryNamed: 'tmp']