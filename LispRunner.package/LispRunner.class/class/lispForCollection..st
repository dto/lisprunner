as yet unclassified
lispForCollection: collection

	| string |
	string := '('.
	collection do: [:item | string := string, (' ', (self lispForValue: item))].
	^ string, ')'