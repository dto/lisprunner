as yet unclassified
lispForValue: value

	^ value isString
		ifTrue: [value]
		ifFalse: [value isCollection ifTrue: [self lispForCollection: value] ifFalse: [value asString]]